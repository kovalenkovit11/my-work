import "./style.css";
import instagram from "./img/instagram.svg";
import twitter from "./img/twitter.svg";
import linkedIn from "./img/linkedIn.svg";
import gitHub from "./img/gitHub.svg";
const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__wrapper">
          <ul className="social">
            <li className="social__item">
              <a href="https://www.instagram.com/" target='_blank' rel="noreferrer">
                <img src={instagram} alt="Link" />
              </a>
            </li>
            <li className="social__item">
              <a href="https://twitter.com/?lang=ru" target='_blank' rel="noreferrer">
                <img src={twitter} alt="Link" />
              </a>
            </li>
            <li className="social__item">
              <a href="https://ua.linkedin.com/" target='_blank' rel="noreferrer">
                <img src={linkedIn} alt="Link" />
              </a>
            </li>
            <li className="social__item">
              <a href="https://github.com/" target='_blank' rel="noreferrer">
                <img src={gitHub} alt="Link" />
              </a>
            </li>
          </ul>
          <div className="copyright">
            {}
            <p>© 2022 frontend</p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
