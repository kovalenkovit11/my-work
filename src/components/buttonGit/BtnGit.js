import './style.css'
import gitHubIcon from './gitHub-black.svg'

const BtnGit = () => {
  return ( 
  <a href='https://github.com/' target="_blank" without rel="noreferrer" className="btn-outline">
  <img src={gitHubIcon} alt=""/>
  GitHub repo
</a>);
}
 
export default BtnGit;