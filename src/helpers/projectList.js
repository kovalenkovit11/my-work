
import firstBig from './img/projects/01-big.jpg'
import secondBig from './img/projects/02-big.jpg'
import thirdBig from './img/projects/03-big.jpg'
import fourBig from './img/projects/04-big.jpg'
import fiveBig from './img/projects/05-big.jpg'
import sixBig from './img/projects/06-big.jpg'
import first from './img/projects/01.jpg'
import second from './img/projects/02.jpg'
import third from './img/projects/03.jpg'
import four from './img/projects/04.jpg'
import five from './img/projects/05.jpg'
import six from './img/projects/06.jpg'

const projects = [
  {
    title: 'Video service',
    skills: 'React, NodeJs, MongoDB',
    img: first,
    imgBig: firstBig,
    gitLubLink: 'https://gitlab.com/' 
  },
  {
    title: 'Video service',
    skills: 'React, NodeJs, MongoDB',
    img: second,
    imgBig: secondBig,
  },
  {
    title: 'Video service',
    skills: 'React, NodeJs, MongoDB',
    img: third,
    imgBig: thirdBig,
    gitLubLink: 'https://gitlab.com/' 
  },
  {
    title: 'Video service',
    skills: 'React, NodeJs, MongoDB',
    img: four,
    imgBig: fourBig, 
  },
  {
    title: 'Video service',
    skills: 'React, NodeJs, MongoDB',
    img: five,
    imgBig: fiveBig
  },
  {
    title: 'Video service',
    skills: 'React, NodeJs, MongoDB',
    img: six,
    imgBig: sixBig,
    gitLubLink: 'https://gitlab.com/' 
  }

]
export {projects}